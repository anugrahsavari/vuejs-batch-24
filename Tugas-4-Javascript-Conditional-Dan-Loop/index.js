//soal 1
var nilai = 80;
var txt;

if (nilai >= 85) {
	txt = 'indexnya A';
} else if (nilai >= 75 && nilai < 85) {
	txt = 'indexnya B';
} else if (nilai >= 65 && nilai < 75) {
	txt = 'indexnya C';
} else if (nilai >= 55 && nilai < 65) {
	txt = 'indexnya D';
} else {
	txt = 'indexnya E';
}
console.log(txt);
//end soal 1

//soal 2
var tanggal = 12;
var bulan = 7;
var tahun = 1998;

var arrayBulan = [
	null,
	'Januari',
	'Februari',
	'Maret',
	'April',
	'Mei',
	'Juni',
	'Juli',
	'Agustus',
	'September',
	'Oktober',
	'November',
	'Desember',
];

for (var i = 1; i <= arrayBulan.length; i++) {
	if (i == bulan) {
		bulan = arrayBulan[i];
	}
}

console.log([tanggal, bulan, tahun].join(' '));
//end soal 2

//soal 3
var n = 7;
var tag = '';

for (var i = 1; i <= n; i++) {
	for (var j = 1; j <= i; j++) {
		tag += '#';
	}
	console.log(tag);
	tag = '';
}
//end soal 3

//soal 4
var m = 10;
var arrayText = [
	null,
	'I Love programming',
	'I Love Javascript',
	'I Love VueJs',
];
var hashtag = '';
for (var i = 1; i <= m; i++) {
	hashtag += '#';
	if (i % 3 == 0) {
		console.log(i + ' - ' + arrayText[3] + '\n' + hashtag);
	} else {
		console.log(i + ' - ' + arrayText[i % 3]);
	}
}
//end soal 4
