// soal 1
var readBooks = require('./callback.js');

var books = [
	{ name: 'LOTR', timeSpent: 3000 },
	{ name: 'Fidas', timeSpent: 2000 },
	{ name: 'Kalkulus', timeSpent: 4000 },
	{ name: 'komik', timeSpent: 1000 },
];
var initTime = 10000;

function waktu(time) {
	readBooks(time, book, callback);
}

books.forEach((book, i) => {
	let time = initTime - book.timeSpent;

	readBooks(initTime, book, waktu(time));
});
