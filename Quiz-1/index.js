//soal 1
var tanggal = 28;
var bulan = 2;
var tahun = 2020;

function next_date(tgl, bln, thn) {
	var arrayBulan = [
		null,
		{ nama: 'Januari', jlh_hari: 31 },
		{ nama: 'Februari', jlh_hari: 28 },
		{ nama: 'Maret', jlh_hari: 31 },
		{ nama: 'April', jlh_hari: 30 },
		{ nama: 'Mei', jlh_hari: 31 },
		{ nama: 'Juni', jlh_hari: 30 },
		{ nama: 'Juli', jlh_hari: 31 },
		{ nama: 'Agustus', jlh_hari: 31 },
		{ nama: 'September', jlh_hari: 30 },
		{ nama: 'Oktober', jlh_hari: 31 },
		{ nama: 'November', jlh_hari: 30 },
		{ nama: 'Desember', jlh_hari: 31 },
	];

	for (var i = 1; i <= arrayBulan.length; i++) {
		if (i == bln) {
			if (i == 2) {
				if (thn % 4 == 0) {
					arrayBulan[i].jlh_hari = 29;
				}
			}
			if (tgl == arrayBulan[i].jlh_hari) {
				tgl = 1;
				bln = arrayBulan[i + 1].nama;
			} else {
				tgl += 1;
				bln = arrayBulan[i].nama;
			}
		}
	}

	return [tgl, bln, thn].join(' ');
}

console.log(next_date(tanggal, bulan, tahun));
//end soal 1

//soal 2
var kalimat_1 = 'Halo nama saya Muhammad Iqbal Mubarok';
var kalimat_2 = 'Saya Iqbal';

function jumlah_kata(string) {
	return string.split(' ').length;
}

console.log(jumlah_kata(kalimat_2));
//end soal 2
