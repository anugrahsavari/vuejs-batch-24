//soal 1
const luasKeliling = (panjang, lebar) => {
	let luas = panjang * lebar;
	let keliling = 2 * (panjang + lebar);
	return `luas: ${luas} \nkeliling: ${keliling}`;
};

console.log(luasKeliling(6, 4));
//end soal 1

// soal 2
const literal = (firstName, lastName) => {
	return `${firstName} ${lastName}`;
};

console.log(literal('William', 'Imoh'));
// end soal 2

// soal 3
const newObject = {
	firstName: 'Muhammad',
	lastName: 'Iqbal Mubarok',
	address: 'Jalan Ranamanyar',
	hobby: 'playing football',
};

const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);
// end soal 3

//soal 4
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];
const combined = [...west, ...east];

console.log(combined);
//end soal 4

// soal 5
const planet = 'earth';
const view = 'glass';

let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;
console.log(after);
// end soal 5
