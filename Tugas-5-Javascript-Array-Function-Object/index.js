//soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
daftarHewan.sort();
daftarHewan.forEach(function (item) {
	console.log(item);
});
//end soal 1

// soal 2
function introduce(element) {
	var arrayElement = [
		'Nama saya ' + element.name,
		'umur saya ' + element.age + ' tahun',
		'alamat saya di ' + element.address,
		'dan saya punya hobby yaitu ' + element.hobby,
	];
	return arrayElement.join(', ');
}
var data = {
	name: 'John',
	age: 30,
	address: 'Jalan Pelesiran',
	hobby: 'Gaming',
};

var perkenalan = introduce(data);
console.log(perkenalan);
// end soal 2

//soal 3
function hitung_huruf_vokal(teks) {
	var countVowel = 0;
	Array.from(teks.toLowerCase()).filter((char) => {
		if (
			char == 'a' ||
			char == 'e' ||
			char == 'i' ||
			char == 'o' ||
			char == 'u'
		) {
			countVowel += 1;
		}
	});
	return countVowel;
	// return teks.match(/[aeiou]/gi).length; //cari huruf vokal menggunakan regex
}

var hitung_1 = hitung_huruf_vokal('Muhammad');

var hitung_2 = hitung_huruf_vokal('Iqbal');

console.log(hitung_1, hitung_2);
// end soal 3

//soal 4
function hitung(angka) {
	var intAngka = parseInt(angka);
	return -2 + intAngka + intAngka;
}

console.log(hitung(5));
// end soal 4
