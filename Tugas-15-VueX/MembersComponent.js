export const MemberComponent = {
	template: '#memberTemplate',
	methods: {
		edit(member) {
			this.$store.dispatch('getMember', member);
		},
		destroy(id) {
			this.$store.dispatch('destroyMember', id);
		},
		showUpload(member) {
			this.$store.dispatch('uploadPhoto', member);
		},
	},
	computed: {
		...Vuex.mapGetters(['members', 'baseUrl']),
	},
};
